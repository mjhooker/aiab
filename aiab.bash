#!/bin/bash


export ASSUME_YES=1
apt-get install -y apt-transport-https
mkdir -p /root/deploy && cd "$_"
git clone https://opendev.org/airship/treasuremap/
cd treasuremap
git fetch https://review.opendev.org/airship/treasuremap refs/changes/66/750866/2 && git checkout FETCH_HEAD
cd /root/deploy/treasuremap/tools/deployment/aiab/
./airship-in-a-bottle.sh